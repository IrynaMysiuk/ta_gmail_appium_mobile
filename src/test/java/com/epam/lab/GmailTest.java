package com.epam.lab;

import com.epam.lab.business_objects.LetterPageBO;
import com.epam.lab.business_objects.LoginPageBO;
import com.epam.lab.business_objects.MainPageBO;
import com.epam.lab.utils.PropertyReader;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import static com.epam.lab.singleton.DriverManager.quitDriver;
import static com.epam.lab.utils.PropertyReader.*;

public class GmailTest {
    MainPageBO mainPageBO;
    LoginPageBO loginPageBO;
    LetterPageBO letterPageBO;


    private GmailTest() {
        mainPageBO = new MainPageBO();
        loginPageBO = new LoginPageBO();
        letterPageBO = new LetterPageBO();
    }

    @Test
    public void gmailTest() {
        PropertyReader propertyReader = new PropertyReader();
        propertyReader.readProperties();
        mainPageBO.goToLogin();
        loginPageBO.logIn(PropertyReader.LOGIN, PropertyReader.PASSWORD);
        Assert.assertEquals(loginPageBO.getProfileName(), PropertyReader.LOGIN,
                "Profile name is not the same");
        loginPageBO.continueLogin();
        mainPageBO.chooseAccount();
        letterPageBO
                .fillLetterFields(RECIPIENT, SUBJECT, MESSAGE_TEXT)
                .goToSentLetters();
        Assert.assertTrue(letterPageBO.getLastMessage().contains("To: ira.mysiuk, Mobile Test, 1111"),
                "Letter is not sent");
    }

    @AfterMethod
    public void closeApp() {
        quitDriver();
    }
}
