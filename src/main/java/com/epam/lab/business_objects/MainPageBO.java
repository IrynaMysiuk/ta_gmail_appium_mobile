package com.epam.lab.business_objects;


import com.epam.lab.page_objects.MainPage;
import io.qameta.allure.Step;

public class MainPageBO {
    private static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(MainPageBO.class);
    private MainPage mainPage;

    public MainPageBO() {
        mainPage = new MainPage();
    }

    @Step("Go to login page")
    public MainPageBO goToLogin() {
        mainPage.getGotItButton().click();
        log.info("Click 'Got it' button");
        mainPage.getNewAccountButton().click();
        log.info("Click 'Add new account' button");
        mainPage.getGoogleItem().click();
        log.info("Choose 'Google'");
        return this;
    }

    @Step("Choose Account")
    public MainPageBO chooseAccount() {
        mainPage.waitCreatedAccount();
        mainPage.getTakeAccountButton().click();
        return this;
    }


}
