package com.epam.lab.business_objects;

import com.epam.lab.page_objects.LetterPage;
import io.qameta.allure.Step;

public class LetterPageBO {
    private static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(MainPageBO.class);
    private LetterPage letterPage;

    public LetterPageBO() {
        letterPage = new LetterPage();
    }

    @Step("Fill letter fields")
    public LetterPageBO fillLetterFields(String recipient, String subject, String message) {
        letterPage.getComposeButton().click();
        log.info("Click 'Compose' button");
        letterPage.getInputRecipient().sendKeys(recipient);
        letterPage.getInputSubject().sendKeys(subject);
        letterPage.getTextArea().sendKeys(message);
        letterPage.getSendButton().click();
        return this;
    }

    @Step("Go to sent letter")
    public LetterPageBO goToSentLetters() {
        letterPage.getOptions().click();
        letterPage.getSentItem().click();
        return this;
    }

    @Step("Verify sent letter")
    public String getLastMessage() {
        return letterPage.getLastSentLetter().getText();
    }
}
