package com.epam.lab.business_objects;

import com.epam.lab.page_objects.LoginPage;
import io.qameta.allure.Step;

public class LoginPageBO {
    private static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(MainPageBO.class);
    private LoginPage loginPage;

    public LoginPageBO() {
        loginPage = new LoginPage();
    }

    @Step("Log In")
    public LoginPageBO logIn(String login, String password) {
        loginPage.getLogin().clear();
        loginPage.getLogin().sendKeys(login);
        log.info("Type login " + login);
        loginPage.getNextLoginButton().click();
        log.info("Click 'Next' button");
        loginPage.getPasswordInput().sendKeys(password);
        log.info("Type login " + password);
        return this;
    }

    @Step("Get profile name")
    public String getProfileName() {
        loginPage.getProfileAccount().click();
        String profileName = loginPage.getProfileAccount().getText();
        log.info("Profile name: " + profileName);
        return profileName;
    }

    @Step("Confirm account")
    public LoginPageBO continueLogin() {
        loginPage.getNextPasswordButton().click();
        log.info("Click 'Next' button");
        loginPage.getIAgreeButton().click();
        log.info("Click 'I agree'");
        return this;
    }

}
