package com.epam.lab.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class PropertyReader {
    public static String LOGIN;
    public static String PASSWORD;
    public static String RECIPIENT;
    public static String SUBJECT;
    public static String MESSAGE_TEXT;

    public void readProperties() {
        Properties properties = new Properties();
        try (FileInputStream fis = new FileInputStream("src/main/resources/credentials.properties")) {
            properties.load(fis);
            initPath(properties);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void initPath(Properties properties) {
        LOGIN = properties.getProperty("login");
        PASSWORD = properties.getProperty("password");
        RECIPIENT = properties.getProperty("to");
        SUBJECT = properties.getProperty("subject");
        MESSAGE_TEXT = properties.getProperty("messageText");
    }
}