package com.epam.lab.utils;

public class Constants {
    public static final int POLLING_TIME = 2;
    public static final int TIME_OUT_IN_SECONDS = 40;

    private Constants(){

    }
}
