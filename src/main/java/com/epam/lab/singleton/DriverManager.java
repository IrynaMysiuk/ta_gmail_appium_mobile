package com.epam.lab.singleton;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.net.MalformedURLException;
import java.net.URL;

import static com.epam.lab.utils.Constants.TIME_OUT_IN_SECONDS;

public class DriverManager {

    private static WebDriverWait wait;
    private static AndroidDriver<MobileElement> driver;

    private DriverManager() {
    }

    public static WebDriverWait getWait() {
        return wait;
    }

    public static AndroidDriver<MobileElement> getDriver() {
        if (driver == null) {
            DesiredCapabilities caps = new DesiredCapabilities();
            caps.setCapability("deviceName", "LG X power");
            caps.setCapability("udid", "LGK220BAIZIRF6");
            caps.setCapability(CapabilityType.PLATFORM_NAME, "Android");
            caps.setCapability(CapabilityType.VERSION, "6.0.1");
            caps.setCapability("appPackage", "com.google.android.gm");
            caps.setCapability("appActivity", "com.google.android.gm.ConversationListActivityGmail");
            try {
                driver = new AndroidDriver<>(new URL("http://127.0.0.1:4723/wd/hub"), caps);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            wait = new WebDriverWait(driver, TIME_OUT_IN_SECONDS);
        }
        return driver;
    }


    public static void quitDriver() {
        if (driver != null) {
            driver.quit();
        }
    }

}