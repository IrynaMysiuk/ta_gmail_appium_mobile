package com.epam.lab.page_objects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LetterPage extends PageObject {
    @FindBy(id = "compose_button")
    private WebElement composeButton;

    @FindBy(id = "to")
    private WebElement inputRecipient;

    @FindBy(id = "subject")
    private WebElement inputSubject;

    @FindBy(id = "composearea_tap_trap_bottom")
    private WebElement textArea;

    @FindBy(id = "send")
    private WebElement sendButton;

    @FindBy(id = "conversation_list_folder_header")
    private WebElement options;

    @FindBy(id = "snippet")
    private WebElement sentItem;

    @FindBy(xpath = "//*[@text=\"1111\"]")
    private WebElement lastSentLetter;


    public WebElement getComposeButton() {
        return getWebElementWithWait(WaitCondition.VISIBILITY, composeButton);
    }

    public WebElement getInputRecipient() {
        return getWebElementWithWait(WaitCondition.VISIBILITY, inputRecipient);
    }

    public void setInputRecipient(WebElement inputRecipient) {
        this.inputRecipient = inputRecipient;
    }

    public WebElement getInputSubject() {
        return getWebElementWithWait(WaitCondition.VISIBILITY, inputSubject);
    }

    public void setInputSubject(WebElement inputSubject) {
        this.inputSubject = inputSubject;
    }

    public WebElement getTextArea() {
        return getWebElementWithWait(WaitCondition.VISIBILITY, textArea);
    }

    public void setTextArea(WebElement textArea) {
        this.textArea = textArea;
    }

    public WebElement getSendButton() {
        return getWebElementWithWait(WaitCondition.VISIBILITY, sendButton);
    }

    public WebElement getOptions() {
        return getWebElementWithWait(WaitCondition.VISIBILITY, options);
    }

    public void setOptions(WebElement options) {
        this.options = options;
    }

    public WebElement getSentItem() {
        return getWebElementWithWait(WaitCondition.VISIBILITY, sentItem);
    }

    public void setSentItem(WebElement sentItem) {
        this.sentItem = sentItem;
    }

    public WebElement getLastSentLetter() {
        return getWebElementWithWait(WaitCondition.VISIBILITY, lastSentLetter);
    }

    public void setLastSentLetter(WebElement lastSentLetter) {
        this.lastSentLetter = lastSentLetter;
    }
}
