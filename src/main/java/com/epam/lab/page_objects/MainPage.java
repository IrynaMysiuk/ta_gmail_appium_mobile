package com.epam.lab.page_objects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class MainPage extends PageObject {


    @FindBy(id = "welcome_tour_got_it")
    private WebElement gotItButton;

    @FindBy(id = "setup_addresses_add_another")
    private WebElement newAccountButton;

    @FindBy(xpath = "//android.widget.LinearLayout[@resource-id='com.google.android.gm:id/account_setup_item' and @index='0']")
    private WebElement googleItem;

    @FindBy(id = "com.google.android.gm:id/action_done")
    private WebElement takeAccountButton;

    @FindBy(id="com.google.android.gm:id/account_display_name")
    private WebElement createdAccount;

    public WebElement getGotItButton() {
        return getWebElementWithWait(WaitCondition.VISIBILITY, gotItButton);
    }

    public void setGotItButton(WebElement gotItButton) {
        this.gotItButton = gotItButton;
    }

    public WebElement getNewAccountButton() {
        return getWebElementWithWait(WaitCondition.VISIBILITY, newAccountButton);
    }

    public void setNewAccountButton(WebElement newAccountButton) {
        this.newAccountButton = newAccountButton;
    }

    public WebElement getGoogleItem() {
        return getWebElementWithWait(WaitCondition.VISIBILITY, googleItem);
    }

    public void setGoogleItem(WebElement googleItem) {
        this.googleItem = googleItem;
    }

    public WebElement getTakeAccountButton() {
        return getWebElementWithWait(WaitCondition.VISIBILITY, takeAccountButton);
    }

    public void setTakeAccountButton(WebElement takeAccountButton) {
        this.takeAccountButton = takeAccountButton;
    }

    public WebElement waitCreatedAccount() {
        return getWebElementWithWait(WaitCondition.VISIBILITY, createdAccount);
    }

    public void setCreatedAccount(WebElement createdAccount) {
        this.createdAccount = createdAccount;
    }
}
