package com.epam.lab.page_objects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.time.Duration;
import java.util.concurrent.TimeoutException;

import static com.epam.lab.singleton.DriverManager.getDriver;
import static com.epam.lab.singleton.DriverManager.getWait;
import static com.epam.lab.utils.Constants.POLLING_TIME;
import static com.epam.lab.utils.Constants.TIME_OUT_IN_SECONDS;

public abstract class PageObject {

    public PageObject() {
        PageFactory.initElements(getDriver(), this);
    }

    protected WebElement getWebElementWithWait(WaitCondition waitCondition, WebElement element) {
        if (waitCondition.equals(WaitCondition.CLICKABLE))
            return getWait().pollingEvery(Duration.ofSeconds(POLLING_TIME))
                    .withTimeout(Duration.ofSeconds(TIME_OUT_IN_SECONDS))
                    .ignoring(TimeoutException.class)
                    .until(ExpectedConditions.elementToBeClickable(element));
        else
            return getWait().pollingEvery(Duration.ofSeconds(POLLING_TIME))
                    .withTimeout(Duration.ofSeconds(TIME_OUT_IN_SECONDS))
                    .ignoring(TimeoutException.class)
                    .until(ExpectedConditions.visibilityOf(element));
    }

    public enum WaitCondition {
        VISIBILITY,
        CLICKABLE
    }
}
