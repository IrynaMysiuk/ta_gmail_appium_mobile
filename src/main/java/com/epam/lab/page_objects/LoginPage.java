package com.epam.lab.page_objects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginPage extends PageObject {
    @FindBy(xpath = "//android.view.View[1]/android.view.View[3]//android.view.View[1]/android.view.View[1]/android.widget.EditText")
    private WebElement loginInput;

    @FindBy(xpath = "//android.widget.Button[@text='Next']")
    private WebElement nextLoginButton;

    @FindBy(xpath = "//*[@resource-id='password']//android.widget.EditText")
    private WebElement passwordInput;

    @FindBy(xpath = "//*[@resource-id='passwordNext']/android.widget.Button")
    private WebElement nextPasswordButton;

    @FindBy(xpath = "//*[@resource-id='signinconsentNext']/android.widget.Button")
    private WebElement iAgreeButton;

    @FindBy(xpath = "//android.widget.Button[@content-desc='More']")
    private WebElement acceptButton;

    @FindBy(xpath = "//android.view.View[@text='seleniumlab12@gmail.com']")
    private WebElement profileAccount;

    @FindBy(xpath = "//android.widget.Button[@text='Accept']")
    private WebElement moreButton;


    public WebElement getLogin() {
        return getWebElementWithWait(WaitCondition.VISIBILITY, loginInput);
    }

    public void setLogin(WebElement loginInput) {
        this.loginInput = loginInput;
    }

    public WebElement getNextLoginButton() {
        return getWebElementWithWait(WaitCondition.VISIBILITY, nextLoginButton);
    }

    public void setNextLoginButton(WebElement nextLoginButton) {
        this.nextLoginButton = nextLoginButton;
    }

    public WebElement getPasswordInput() {
        return getWebElementWithWait(WaitCondition.VISIBILITY, passwordInput);
    }

    public void setPasswordInput(WebElement passwordInput) {
        this.passwordInput = passwordInput;
    }

    public WebElement getIAgreeButton() {
        return getWebElementWithWait(WaitCondition.VISIBILITY, iAgreeButton);
    }

    public void setIAgreeButton(WebElement iAgreeButton) {
        this.iAgreeButton = iAgreeButton;
    }

    public WebElement getAcceptButton() {
        return getWebElementWithWait(WaitCondition.VISIBILITY, acceptButton);
    }

    public void setAcceptButton(WebElement acceptButton) {
        this.acceptButton = acceptButton;
    }

    public WebElement getProfileAccount() {
        return getWebElementWithWait(WaitCondition.VISIBILITY, profileAccount);
    }

    public void setProfileAccount(WebElement profileAccount) {
        this.profileAccount = profileAccount;
    }


    public WebElement getMoreButton() {
        return getWebElementWithWait(WaitCondition.VISIBILITY, moreButton);
    }

    public void setMoreButton(WebElement moreButton) {
        this.moreButton = moreButton;
    }


    public WebElement getNextPasswordButton() {
        return getWebElementWithWait(WaitCondition.VISIBILITY, nextPasswordButton);
    }

    public void setNextPasswordButton(WebElement nextPasswordButton) {
        this.nextPasswordButton = nextPasswordButton;
    }
}
