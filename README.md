# Gmail Appium Mobile Automation Tests / TA_Gmail_Appium_Mobile

## About

* It is testing of Gmail page using Appium, Selenium and TestNG
* Project status: working

## Project Structure
1. page_objects - low level logic, path to locators separated in suitable classes
2. bo - business object, common high level logic
3. singleton - Selenium WebDriver with Singleton pattern
4. utils - additional package 

### Content
**Test steps:**

1. Open gmail & login
1. Click on compose button
1. Enter correct email in “to” field 
1. Fill “subject”/”message” fields & press “send” button 
1. Verify that message is moved to “Sent mail” folder

### Configuration
Look at `src/main/resources/path.properties`

| Name | Default value | Description |
| ------------- | ------------- | ---|
| platform.name  | Android | Platform name |
| platform.version  | 6.0.1  | Platform version |
| app.package.name |com.google.android.gm| Application name |
| app.package.path |com.google.android.gm.ConversationListActivityGmail|Application path|

### Reports

After running tests reports are generated in:

* Allure report `allure-results`
Note: execute command `allure serve allure-results`
* TestNG report `target/surefire-reports/index.html`
* Emailable report `targer/surefire-reports/emailable-report.html`

### Build

    mvn clean test
Generate Allure report: 

    allure serve allure-results 
    
### Real device configuration
In order to use the real device we should do the followings:
- Connect your real device to your laptop via USB port.
- Go to Settings > Developer Settings and enable USB Debugging option.

Set `udid` according the result from the command:

    C:\Users\Mysiuk>adb devices
    List of devices attached
    LGK220BAIZIRF6  device

Set `app.name` and `app.path` according the result from the command: 

    C:\Users\Mysiuk>adb shell
    shell@mk6p:/ $ dumpsys window windows | grep -E 'mCurrentFocus'
      mCurrentFocus=Window{3de47cc u0 com.google.android.gm/com.google.android.gm.ConversationListActivityGmail}
    
### Device

There is used a real device: 
   
    LG X Power K220
    

### Developer 
 Iryna Mysiuk - https://bitbucket.org/IrynaMysiuk/